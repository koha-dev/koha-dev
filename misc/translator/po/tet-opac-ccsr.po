# LibLime Koha Translation Manager
# Copyright (C) 2007 LibLime
# http://liblime.com <info@liblime.com>
# Based on Kartouche http://www.dotmon.com/kartouche/
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-05-05 21:48-0300\n"
"PO-Revision-Date: 2014-03-07 02:43+0000\n"
"Last-Translator: kaz <karenmyers1@hotmail.com>\n"
"Language-Team: Koha Translation Team <koha-translate@nongnu.org>\n"
"Language: tet\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.5.0\n"
"X-POOTLE-MTIME: 1394160221.0\n"

#. %1$s:  IF ( related ) 
#. %2$s:  FOREACH relate IN related 
#. %3$s:  relate.related_search 
#. %4$s:  END 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:229
#, c-format
msgid "%s (related searches: %s%s%s). %s "
msgstr "%s (peskiza relevante: %s%s%s). %s "

#. %1$s:  ELSE 
#. %2$s:  END 
#. %3$s:  IF ( opacuserlogin ) 
#: opac-tmpl/ccsr/en/includes/top-bar.inc:24
#, c-format
msgid "%s No public lists %s %s "
msgstr "%s Lista publiku la iha %s %s "

#. %1$s:  IF ( searchdesc ) 
#. %2$s:  LibraryName 
#: opac-tmpl/ccsr/en/includes/masthead.inc:235
#, c-format
msgid "%s No results found for that in %s catalog. "
msgstr "%s La hetan rezultadu husi %s katalogu. "

#. %1$s:  ELSE 
#. %2$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:237
#, c-format
msgid "%s You did not specify any search criteria. %s "
msgstr "%s Ita la espesifika kriteria ida atu peskiza. %s "

#. %1$s:  END 
#: opac-tmpl/ccsr/en/includes/top-bar.inc:66
#, c-format
msgid "%sLog Out"
msgstr "%sLog Out"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Add to your cart"
msgstr "Aumenta ba ita-nia karriñu"

#: opac-tmpl/ccsr/en/includes/masthead.inc:194
#, c-format
msgid "Advanced search"
msgstr "Peskiza avansadu"

#: opac-tmpl/ccsr/en/includes/masthead.inc:180
#, c-format
msgid "All Libraries"
msgstr "Biblioteka Hotu"

#: opac-tmpl/ccsr/en/includes/masthead.inc:105
#, c-format
msgid "All libraries"
msgstr "Biblioteka hotu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Are you sure you want to empty your cart?"
msgstr "Ita hakarak hamamuk ita nia karriñu ka?"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Are you sure you want to remove the selected items?"
msgstr "Ita hakarak hasai item sira ne'ebe ita hili tiona ka?"

#: opac-tmpl/ccsr/en/includes/masthead.inc:63
#: opac-tmpl/ccsr/en/includes/masthead.inc:65
#, c-format
msgid "Author"
msgstr "Autor"

#: opac-tmpl/ccsr/en/includes/masthead.inc:197
#, c-format
msgid "Authority search"
msgstr "Peskiza autoridade"

#: opac-tmpl/ccsr/en/includes/masthead.inc:196
#, c-format
msgid "Browse by hierarchy"
msgstr "Haree tuir hierarquia"

#: opac-tmpl/ccsr/en/includes/masthead.inc:83
#: opac-tmpl/ccsr/en/includes/masthead.inc:85
#, c-format
msgid "Call number"
msgstr "Kota"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Cart"
msgstr "Karriñu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:97
msgid ""
"Could not login, perhaps your Persona email does not match your Koha one"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:195
#, c-format
msgid "Course reserves"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! Illegal parameter"
msgstr "Sala! Parameter ilegál"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! The add_tag operation failed on"
msgstr "Sala! Operasaun add_tag la konsege"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! You cannot delete the tag"
msgstr "Sala! Labele hamoos tag ne'e"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Error! Your tag was entirely markup code. It was NOT added. Please try again "
"with plain text."
msgstr ""
"Sala! Ita nia tag mak kodigu markup deit. Ida ne'e LABELE aumenta. Favor "
"koko fali ho testu simples deit."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Errors: "
msgstr "Sala: "

#. INPUT type=submit
#: opac-tmpl/ccsr/en/includes/masthead.inc:139
#: opac-tmpl/ccsr/en/includes/masthead.inc:186
msgid "Go"
msgstr "Ba"

#. OPTGROUP
#: opac-tmpl/ccsr/en/includes/masthead.inc:113
msgid "Groups"
msgstr "Grupu sira"

#. A
#: opac-tmpl/ccsr/en/includes/masthead.inc:227
#, c-format
msgid "Home"
msgstr "Uma"

#: opac-tmpl/ccsr/en/includes/masthead.inc:73
#: opac-tmpl/ccsr/en/includes/masthead.inc:75
#, c-format
msgid "ISBN"
msgstr "ISBN"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "In your cart"
msgstr "Iha ita nia karriñu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Items in your cart: "
msgstr "Items iha ita nia karriñu: "

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:14
#: opac-tmpl/ccsr/en/includes/masthead.inc:20
msgid "Koha Online Catalog"
msgstr "Katalogu Koha Onlines"

#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:3
msgid "Koha [% Version %]"
msgstr "Koha [% Versaun %]"

#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:42
#, c-format
msgid "Languages:&nbsp;"
msgstr "Lian sira:&nbsp;"

#. OPTGROUP
#: opac-tmpl/ccsr/en/includes/masthead.inc:106
msgid "Libraries"
msgstr "Biblioteka sira"

#: opac-tmpl/ccsr/en/includes/masthead.inc:165
#, c-format
msgid "Library Catalog"
msgstr "Katalogu Biblioteka"

#: opac-tmpl/ccsr/en/includes/masthead.inc:53
#: opac-tmpl/ccsr/en/includes/masthead.inc:55
#, c-format
msgid "Library catalog"
msgstr "Katalogu biblioteka"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:14
#, c-format
msgid "Lists"
msgstr "Lista sira"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:44
#, c-format
msgid "Log in to create your own lists"
msgstr "Log in hodu kria ita-nia lista sira"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:58
#, c-format
msgid "Log in to your account"
msgstr "Log in ba ita-nia konta"

#: opac-tmpl/ccsr/en/includes/masthead.inc:201
#, c-format
msgid "Most popular"
msgstr "Popular liu"

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:45
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:52
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:108
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:116
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:125
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:133
msgid "No cover image available"
msgstr "Livru oan nia imajen la disponivel"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "No item was added to your cart"
msgstr "La aumenta item ida ba ita nia karriñu"

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "No item was selected"
msgstr "La hili item ida"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:40
#, c-format
msgid "No private lists"
msgstr "Lista privadu la iha"

#: opac-tmpl/ccsr/en/includes/masthead.inc:234
#, c-format
msgid "No results found!"
msgstr "La hetan rezultadu!"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "No tag was specified."
msgstr "La espesifika tag ida."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Note: you can only delete your own tags."
msgstr "Avizu: ita bele hamoos ita nia tags deit."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Note: you can only tag an item with a given term once. Check 'My Tags' to "
"see your current tags."
msgstr ""
"Nota: ita bele tag item ida ho termus dala ida deit. Verifika iha 'My Tags' "
"atu haree ita nia tags daudaun."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Note: your tag contained markup code that was removed. The tag was added as "
msgstr ""
"Avisu: ita nia tag iha kodigu markup ne'ebe hasai tiona. Tag ne'e aumenta "
"hanesan "

#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:28
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:30
#, c-format
msgid "Powered by"
msgstr "Powered husi"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:17
#, c-format
msgid "Public lists"
msgstr "Lista publiku"

#: opac-tmpl/ccsr/en/includes/masthead.inc:203
#: opac-tmpl/ccsr/en/includes/masthead.inc:204
#, c-format
msgid "Purchase suggestions"
msgstr "Sujestaun ida kona-ba livru ida biblioteka mak sosa"

#: opac-tmpl/ccsr/en/includes/masthead.inc:198
#, c-format
msgid "Recent comments"
msgstr "Komentariu sira foun daudaun"

#. A
#: opac-tmpl/ccsr/en/includes/masthead.inc:228
#, c-format
msgid "Search"
msgstr "Peskiza"

#. For the first occurrence,
#. %1$s:  UNLESS ( OpacAddMastheadLibraryPulldown ) 
#. %2$s:  IF ( mylibraryfirst ) 
#. %3$s:  mylibraryfirst 
#. %4$s:  END 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:41
#: opac-tmpl/ccsr/en/includes/masthead.inc:154
#, c-format
msgid "Search %s %s (in %s only)%s %s "
msgstr "Peskiza %s %s (iha %s deit)%s %s "

#: opac-tmpl/ccsr/en/includes/top-bar.inc:64
#, c-format
msgid "Search history"
msgstr "Istoria peskiza"

#: opac-tmpl/ccsr/en/includes/masthead.inc:78
#: opac-tmpl/ccsr/en/includes/masthead.inc:80
#, c-format
msgid "Series"
msgstr "Serie sira"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Sorry, tags are not enabled on this system."
msgstr "Deskulpa, tags la lao iha sistema ida ne'e."

#: opac-tmpl/ccsr/en/includes/masthead.inc:68
#: opac-tmpl/ccsr/en/includes/masthead.inc:70
#, c-format
msgid "Subject"
msgstr "Asuntu"

#: opac-tmpl/ccsr/en/includes/masthead.inc:200
#, c-format
msgid "Subject cloud"
msgstr "Kalohan asuntu"

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:232
#: opac-tmpl/ccsr/en/includes/masthead.inc:237
msgid "Subscribe to this search"
msgstr "Asina ba peskiza ida ne'e"

#: opac-tmpl/ccsr/en/includes/masthead.inc:199
#, c-format
msgid "Tag cloud"
msgstr "Tag kalohan"

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Tags added: "
msgstr "Tags aumenta tiona: "

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "This item has been added to your cart"
msgstr "Item ida ne'e aumenta tiona ba ita-nia karriñu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "This item has been removed from your cart"
msgstr "Item ida ne'e hasai tiona husi ita-nia karriñu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "This item is already in your cart"
msgstr "Item ida ne'e iha ona iha ita nia karriñu"

#: opac-tmpl/ccsr/en/includes/masthead.inc:58
#: opac-tmpl/ccsr/en/includes/masthead.inc:60
#, c-format
msgid "Title"
msgstr "Titulu"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Unable to add one or more tags."
msgstr "La konsege atu aumenta tag ida ka liu."

#. A
#: opac-tmpl/ccsr/en/includes/top-bar.inc:64
msgid "View your search history"
msgstr "Haree ita nia istoria peskiza"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:60
#, c-format
msgid "Welcome, "
msgstr "Bemvindo, "

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "You must be logged in to add tags."
msgstr "Tenke log in atu aumenta tags."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Your cart is currently empty"
msgstr "Ita nia karriñu mamuk agora daudaun"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:6
#, c-format
msgid "Your cart is empty."
msgstr "Ita nia lista"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:29
#, c-format
msgid "Your lists"
msgstr "Ita nia lista"

#. %1$s:  total |html 
#: opac-tmpl/ccsr/en/includes/masthead.inc:229
#, c-format
msgid "Your search returned %s results."
msgstr "Ita nia peskiza hetan tiona rezultadu %s."

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:16
#: opac-tmpl/ccsr/en/includes/masthead.inc:22
msgid "[% LibraryName %] Online Catalog"
msgstr "[% LibraryName %] Katalogu Online"

#. INPUT type=text name=q
#: opac-tmpl/ccsr/en/includes/masthead.inc:91
msgid "[% ms_value |html %]"
msgstr "[% ms_value |html %]"

#. %1$s:  INCLUDE 'top-bar.inc' 
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:12
#, c-format
msgid ""
"[%%# Sticking the div for the top bar here; since the top bar is positioned "
"absolutely in this theme, it it makes the positioning of the rest of the "
"elements easier to keep it out of the doc3 div. %%] %s "
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:41
#, c-format
msgid "[New list]"
msgstr "[Lista foun]"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:23
#, c-format
msgid "[View All]"
msgstr "[Haree Hotu]"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:36
#, c-format
msgid "[View all]"
msgstr "[Haree hotu]"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "already in your cart"
msgstr "iha ona iha ita nia karriñu"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:13
#, c-format
msgid "change my password"
msgstr "troka hau nia password"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "item(s) added to your cart"
msgstr "item(s) aumenta tiona ba ita-nia karriñu"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:6
#, c-format
msgid "my fines"
msgstr "hau nia multa"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:33
#, c-format
msgid "my lists"
msgstr "hau nia lista sira"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:30
#, c-format
msgid "my messaging"
msgstr "hau nia sistema mensajen"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:8
#, c-format
msgid "my personal details"
msgstr "hau nia detalle sira pesoal"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:21
#, c-format
msgid "my privacy"
msgstr "hau nia privasidade"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:26
#, c-format
msgid "my purchase suggestions"
msgstr "hau nia sujestaun kona-ba livru atu sosa"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:19
#, c-format
msgid "my reading history"
msgstr "hau nia lista livru ne'ebe hau lee ona"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:16
#, c-format
msgid "my search history"
msgstr "hau nia istoria peskiza"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:4
#, c-format
msgid "my summary"
msgstr "hau nia rezumu"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:10
#, c-format
msgid "my tags"
msgstr "hau nia tag sira"

#. META http-equiv=Content-Type
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:2
msgid "text/html; charset=utf-8"
msgstr "text/html; charset=utf-8"

#. LINK
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:141
msgid "unAPI"
msgstr "unAPI"

#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:25
msgid ""
"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,"
"user-scalable=no"
msgstr ""
"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,"
"user-scalable=no"
